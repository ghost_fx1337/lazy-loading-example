const allImages = document.querySelectorAll('img');

const lazyLoad = target => {
    const io = new IntersectionObserver((entries, observer) => {
        entries.forEach(entry => {
            if(entry.isIntersecting) {
                const img = entry.target;
                const src = img.getAttribute('data-lazy');
                img.setAttribute('src', src);
                img.classList.remove('addon');
                img.classList.add('addon');

                observer.disconnect();  
            }
        })
    });
    io.observe(target);
}

allImages.forEach(lazyLoad);